-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 17, 2017 at 10:37 AM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `medicvn`
--

-- --------------------------------------------------------

--
-- Table structure for table `deals`
--

CREATE TABLE IF NOT EXISTS `deals` (
  `deal_id` int(11) NOT NULL,
  `deal_title` text COLLATE utf8_unicode_ci NOT NULL,
  `deal_content` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `old_price` decimal(10,0) NOT NULL,
  `end_time` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `ranked` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `deals`
--

INSERT INTO `deals` (`deal_id`, `deal_title`, `deal_content`, `image`, `price`, `old_price`, `end_time`, `created_at`, `updated_at`, `ranked`) VALUES
(1, 'Khám chuyên khoa Xương - Khớp - Cột sống', '<p style="text-align:justify"><strong>Giới thiệu dịch vụ</strong></p>\r\n\r\n<p style="text-align:justify"><strong>Quy trình dịch vụ:</strong></p>\r\n\r\n<ul>\r\n	<li style="text-align:justify">\r\n	<p>Bước 1: Tư vấn</p>\r\n	</li>\r\n	<li style="text-align:justify">\r\n	<p>Bước 2: Thăm khám</p>\r\n	</li>\r\n</ul>\r\n\r\n<p style="text-align:justify">Kiểm tra các chỉ số cơ bản của cơ thế</p>\r\n\r\n<p style="text-align:justify">Gặp bác sĩ để thăm khám</p>\r\n\r\n<p style="text-align:justify">Bác sĩ đưa ra lộ trình và giải pháp cho từng bệnh nhân</p>\r\n\r\n<ul>\r\n	<li style="text-align:justify">\r\n	<p>Bước 3: Điều trị</p>\r\n	</li>\r\n</ul>\r\n\r\n<p style="text-align:justify">Các kĩ thuật viên điều trị theo phác đồ của bác sĩ</p>\r\n\r\n<ul>\r\n	<li style="text-align:justify">\r\n	<p>Bước 4:Chăm sóc</p>\r\n	</li>\r\n</ul>\r\n\r\n<p style="text-align:justify"><strong>Giới thiệu</strong>&nbsp;<strong>Phòng khám Cơ Xương Khớp Cột sống - Bác sĩ Nguyễn Ngọc Tuấn</strong></p>\r\n\r\n<p style="text-align:justify">Bác sĩ Nguyễn Ngọc Tuấn tốt nghiệp thủ khoa&nbsp;Đại học y khoa Huế khóa 1997-2003. Tốt nghiệp Thạc sĩ - chuyên ngành Chấn Thương Chỉnh Hình -&nbsp;Đại học Y Dược TP.HCM&nbsp;2007-2009.</p>\r\n\r\n<p style="text-align:justify">Với kiến thức&nbsp;và kinh nghiệm làm việc của mình,&nbsp;Bác sĩ Nguyễn Ngọc Tuấn thành lập&nbsp;Phòng khám Cơ Xương Khớp Cột sống - là một địa chỉ tin cậy cho người bệnh ở TP. HCM</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p style="text-align:justify">Sở hữu những công nghệ tiên tiến&nbsp;</p>\r\n	</li>\r\n	<li>\r\n	<p style="text-align:justify">Đội ngũ bác sỹ giỏi, có đạo đức nghề nghiệp</p>\r\n	</li>\r\n	<li>\r\n	<p style="text-align:justify">Chăm sóc chu&nbsp; đáo, ân cần, kỹ lưỡng từ các bác sỹ, nhân viên y tế &nbsp;</p>\r\n	</li>\r\n</ul>\r\n\r\n<p style="text-align:justify">Quý khách sẽ dễ dàng cảm nhận được sự ân cần, nhiệt tâm và hướng dẫn tận tình của đội ngũ y bác sỹ của Da liễu Hồng Đức ngay từ lần đầu tiên trải nghiệm dịch vụ nơi đây.</p>				', '10_01_2017_06_15_15_540422.jpeg', '70000', '100000', '2018-12-31 00:00:00', '2017-01-17 00:00:00', '0000-00-00 00:00:00', 1),
(2, 'Lấy vôi răng hai hàm', '<p style="text-align:justify"><strong>Giới thiệu dịch&nbsp;vụ</strong></p>\r\n\r\n<p style="text-align:justify">Vôi răng bám trên bề mặt răng chiếm lấy vị trí của nướu làm viền nướu tụt xuống về phía chân răng và ngày càng nhiều vôi thì hiện tượng tiêu xương xảy ra. Vi khuẩn bám trên răng ngày càng nhiều, nướu viêm dễ chảy máu khi chải răng, hơi thở có mùi hôi. Và để càng lâu thì vôi răng ngày càng lớn, nướu tụt xuống chân răng càng sâu, răng bắt đầu có hiện tượng lung lay và dễ dẫn tới mất răng sớm. Như vậy việc cạo vôi răng và đánh bóng định kỳ rất quan trọng. Chúng ngăn ngừa bệnh lý viêm nướu, viêm nha chu xảy ra, và hạn chế sự tích tụ vi khuẩn, mảng bám thức ăn dễ gây sâu răng.</p>\r\n\r\n<p style="text-align:justify">Bên cạnh đó,&nbsp;việc lấy&nbsp;vôi răng định kỳ giúp bác sĩ loại bỏ được các mảng bám thức ăn, vôi răng vết dính, việc làm này còn giúp dễ dàng phát hiện những lỗ sâu mới chớm. Sâu răng chỉ được trám khi nào? Khi sâu răng có lỗ sâu vướng thám trâm khi thăm khám, ở đáy xoang có mùn ngà mềm và mảnh vụn thức ăn tích tụ trong lỗ sâu. Những tác nhân này cần được loại bỏ sớm và thay vào đó là các vật liệu nha khoa trám răng nhằm ngăn chặn sâu răng tiến triển, ngăn ngừa những kích thích nóng lạnh, chua ngọt ảnh hưởng đến tủy răng bên trong. Răng còn tủy giúp sự sống của răng trường tồn hơn.</p>\r\n\r\n<p style="text-align:justify">Để khắc phục tình trạng này, <strong><em>Nha khoa HaNa mang đến cho bạn dịch&nbsp; vụ "Lấy&nbsp;vôi răng hai hàm"</em></strong>. Với sự tỉ mỉ, nhẹ nhàng và những vùng vôi dưới nướu ẩn mình sẽ được các bác sĩ làm sạch, nhanh chóng trả lại vẻ trắng bóng, chắc khỏe cho răng. Chỉ với&nbsp;150.000 VNĐ, bạn có thể tận hưởng dịch vụ chăm sóc răng miệng chuyên nghiệp đến từ Nha khoa HaNa.</p>\r\n\r\n<p style="text-align:justify"><strong>Giới thiệu Nha khoa HaNa</strong></p>\r\n\r\n<p style="text-align:justify">Với đội ngũ Y Bác sĩ trường Đại học Y Dược Tp Hồ Chí Minh, làm việc tại các bệnh viện với bề dày kinh nghiệm, nha khoa HaNa tự tin có thể làm hài lòng ngay cả những khách hàng khó tính nhất.</p>\r\n\r\n<p style="text-align:justify">Nha khoa được trang bị đầy đủ các phương tiện chẩn đoán trang thiết bị hiện đại, vô trùng tuyệt đối, đảm bảo an toàn sức khỏe cho khách hàng.</p>\r\n\r\n<p style="text-align:justify">Không gian phòng khám rộng rãi, thoáng mát, được vô trùng kỹ lưỡng, tạo cảm giác thoải mái và an tâm nhất cho khách hàng khi chăm sóc răng tại đây.</p>\r\n\r\n					', '01_11_2016_08_15_03_646935.jpeg', '150000', '300000', '2017-05-10 23:59:59', '2017-01-17 10:00:00', '2017-01-17 10:00:00', 1),
(3, 'Uốn - Duỗi tóc cao cấp', 'Địa điểm áp dụng: Salon Kao Profesional - 15 Bùi Đình Túy, P. 26, Q. Bình Thạnh, TP.HCM\r\n\r\nThời hạn sử dụng: 29/01/2017\r\n\r\nThời gian sử dụng: Tất cả các ngày trong tuần. 9h đến 20h\r\n\r\nĐối tượng sử dụng: Khách hàng đến từ ViCare\r\n\r\nĐiều kiện khác: Quý khách nhận ưu đãi tại ViCare.vn và thanh toán trực tiếp tại Salon Kao Profesional\r\n\r\nQuý khách vui lòng liên hệ trước để được phục vụ tốt nhất!', '08_11_2016_07_24_22_021345.jpeg', '350000', '700000', '2017-01-29 23:00:00', '2017-01-17 05:00:00', '2017-01-17 05:00:00', 1),
(4, 'Giảm 10% với các dịch vụ Implant', 'Địa điểm áp dụng: Nha khoa HaNa - Số 151 Lương Định Của, phường Bình An, Quận 2, TP. HCM\r\n\r\nĐối tượng áp dụng: Tất cả khách hàng của ViCare\r\n\r\nĐiều kiện khác: Khách hàng nhận mã ưu đãi tại ViCare.vn và thanh toán trực tiếp tại Nha khoa\r\n\r\nVui lòng đặt chỗ trước khi đến để được phục vụ tốt nhất.', '28_09_2016_03_51_26_458584.jpeg', '160000', '180000', '2017-05-10 23:00:00', '2017-01-17 09:00:00', '2017-01-17 10:00:00', 1),
(5, 'Xét nghiệm tại nhà', 'Địa điểm áp dụng: Trung tâm xét nghiệm Tasscare - 227 Đường 9A Khu Trung Sơn, Nam Sài Gòn, Hồ Chí Minh\r\n\r\nThời hạn sử dụng: 31/03/2017\r\n\r\nĐối tượng áp dụng: Khách hàng tới từ ViCare\r\n\r\nNgày áp dụng trong tuần: Tất cả các ngày trong tuần\r\n\r\nThời gian áp dụng: 8h00 - 20h00 \r\n\r\nĐiều kiện khác: Khác hàng nhận ưu đãi tại ViCare.vn và thanh toán trực tiếp tại Trung tâm \r\n\r\nKhách hàng vui lòng đặt chỗ trước khi đến để được phục vụ tốt nhất.', '05_01_2017_05_16_04_029149.jpeg', '0', '0', '2017-03-31 13:00:00', '2017-01-17 10:00:00', '2017-01-17 07:00:00', 1),
(6, 'Trẻ hóa trắng sáng da mặt cấp tốc với thảo dược, thuốc Bắc và Serum đặc trị sáng da của Pháp', 'Địa điểm áp dụng: Thảo Spa - 240/1B Cách Mạng Tháng 8, P.10, Q.3\r\n\r\nThời hạn sử dụng: 04/01/2017 đến 28/02/2017\r\n\r\nThời gian sử dụng: Tất cả các ngày trong tuần. 9h đến 20h\r\n\r\nĐối tượng sử dụng: Khách hàng đến từ ViCare.\r\n\r\nĐiều kiện khác: Khách hàng nhận ưu đãi tại ViCare.vn và thanh toán trực tiếp tại Thảo Spa\r\n\r\nQuý khách vui lòng liên hệ trước để được phục vụ tốt nhất!', '21_10_2016_10_07_42_661541.jpeg', '198000', '900000', '2017-02-28 00:00:00', '2017-01-17 06:00:00', '2017-01-17 10:00:00', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `deals`
--
ALTER TABLE `deals`
  ADD PRIMARY KEY (`deal_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `deals`
--
ALTER TABLE `deals`
  MODIFY `deal_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
